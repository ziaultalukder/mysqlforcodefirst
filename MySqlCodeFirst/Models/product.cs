namespace MySqlCodeFirst.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("test.product")]
    public partial class product
    {
        public int ProductId { get; set; }

        [StringLength(45)]
        public string Name { get; set; }
    }
}
